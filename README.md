![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

## slackrepo

This is the git repository for [idlemoor.github.io/slackrepo](https://idlemoor.github.io/slackrepo),
documentation for slackrepo, a package builder for SlackBuilds.

It lives on Gitlab ;)

Note, to view markdown source files on Gitlab,
you need to click the tiny 'Display source' button,
which looks like [</>]
