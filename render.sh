#!/bin/sh

set -e

for manpage in slackrepo.conf.5 slackrepo.hint.5 slackrepo.8; do
  mandoc -Thtml -Ofragment mansrc/"${manpage}" > jekyll/_includes/man-"${manpage}".html
done

jekyll build

if [ "$1" = '--pkg' ]; then
  rsync -Pva --delete packages/ root@vhs.idlemoor.tk:/Web/files/packages/
fi

exit 0
