---
layout: default
title: Introduction
permalink: /
addfooter: foot-disclaimer.html
---

<div class="floatleft">
<h1 id="introduction">Introduction</h1>
<p style="font-size:large">
Slackrepo is a highly automated tool for Slackware users to manage
their own package repositories. It builds packages from a repository of
SlackBuilds (for example, a clone of
<a class="ext" href="https://SlackBuilds.org">SlackBuilds.org</a>),
and then outputs the packages to a package repository.
</p>
<p style="font-size:large">
See the <a href="{{ site.baseurl }}/quickstart.html">Quick start</a> and
<a href="{{ site.baseurl }}/download.html">Download</a> pages for more information.
</p>
</div>

<div class="floatright">
<a href="{{ site.baseurl }}/screenshots.html">
<img src="{{ site.baseurl }}/screenshots/screenmate.png" alt="screenshot of mate being built" title="mate being built">
</a>
</div>

<div style="clear: both"></div>

## New release 0.3, September 2017

<p style="font-size:large">
    After a long time in development, here is a new release for you!
</p>

<b>Important! The default hintfiles are now in a separate package.</b>
<br />Here are the two prebuilt packages that you need:
<br /><b><a class="ext" href="https://files.idlemoor.tk/packages/slackrepo-0.3-noarch-1_dbs.txz">slackrepo-0.3-noarch-1_dbs.txz</a></b>
<br /><b><a class="ext" href="https://files.idlemoor.tk/packages/slackrepo-hintfiles-20170826-noarch-1_dbs.txz">slackrepo-hintfiles-20170826-noarch-1_dbs.txz</a></b>
<br />See the <a href="{{ site.baseurl }}/download.html">Download</a> page for signatures and md5sums.

New features in this release include dependency control
(<code>SUBSTITUTE</code>, <code>DELREQUIRES</code>), resource monitoring,
start and finish hooks, preview mode,
Python 3 injection, and more lint checks with suppression of warnings.
Bug fixes include <code>--install</code>, temp file usage and mount handling.
See the <a href="{{ site.baseurl }}/news.html">News</a> page for more details.
