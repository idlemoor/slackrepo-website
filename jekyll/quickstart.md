---
layout: default
title: Quick Start
---

# Quick Start

<p style="font-size:larger">Slackrepo is a highly automated tool for Slackware users to manage their own
package repositories.</p>

<p style="font-size:larger">It builds packages from a repository of SlackBuilds
and then outputs the packages to a package repository.</p>

<p style="font-size:larger">Some notable features are:</p>

  * <b>Automatic updating</b> &mdash;
    revisions of the SlackBuild repository determine <a href="{{ site.baseurl }}/doc/usage.html#updates">updates</a> and rebuilds of packages
  * <b>Clean building</b> &mdash;
    <a href="{{ site.baseurl }}/doc/usage.html#dependencies">dependencies</a> are built as a 'tree', not a linear 'queue',
    and packages that are out-of-date with respect to their dependencies are automatically rebuilt;
    slackrepo creates a throw-away chroot environment for each build, to help keep your build box safe and clean.
  * <b>No interaction required</b> &mdash;
    highly automated and suitable for batch building hundreds or even thousands of packages
  * Can build from
    <b><a class="ext" href="https://SlackBuilds.org">SlackBuilds.org</a> (SBo)</b>,
    Chess Griffin and Willy Sudiarto Raharjo's
    <b><a class="ext" href="https://github.com/mateslackbuilds/msb">Mate SlackBuilds</a> (msb)</b>,
    Willy's
    <b><a class="ext" href="https://github.com/willysr/csb">Cinnamon SlackBuilds</a> (csb),</b>
    and Matteo Bernardini's forked
    <b><a class="ext" href="https://github.com/Ponce/slackbuilds">SBo repository for slackware-current</a> (ponce).</b>
  * <b>Quality assurance checks</b> &mdash;
    optional <a href="{{ site.baseurl }}/doc/checks.html">lint checks</a> can be performed on the SlackBuild files and built packages


## Running

Slackrepo runs from the command line.
It is not interactive and does not have a graphical interface.
You can run it as the root user, or (if you do some additional setup) as a non-root user.
Sources are downloaded on demand (unless already available in the local cache);
a well-functioning network connection is assumed.

For best results, slackrepo is intended to be run on a "clean" standard
Slackware environment (like a "build box" or a virtual machine), with a
full installation of Slackware and no additional packages (or very
few).  You can then use the package repository to install the packages on
other hosts (e.g. with <a class="ext" href="https://github.com/zuno/slackpkgplus">slackpkg+</a>).
However, slackrepo can also be used as a simple local command-line package builder and installer.

<p class="scary">Before you use slackrepo to build packages,
you should read and understand the page about <b><a href="{{ site.baseurl }}/doc/hintfiles.html">hintfiles</a></b>.
You should always check the README of each SlackBuild.</p>


## Examples

```
# Build graphics/shotwell, with all its dependencies
slackrepo build graphics/shotwell
# or just
slackrepo build shotwell

# Build and install shotwell and all its dependencies
# (note, the system will no longer be a clean build environment)
slackrepo build --install shotwell
```

```
# Build the whole SBo repository!
# (you will need at least four days and 80Gb of disk space)
slackrepo build
```

```
# Update all the academic/ packages in your package repository for SBo's latest changes
# (this WILL NOT add new packages, unless they are required dependencies)
slackrepo update academic

# Build all the academic/ packages in SBo
# (this WILL add new packages, and will also update for SBo's latest changes)
slackrepo build academic
```

```
# Do a "preview" update of all your existing SBo packages
# (shows you what would be updated, rebuilt and removed)
slackrepo update --preview
```

```
# Build colord (in the csb repo) with lint checks, do not store the built package,
# and keep all the temporary files so you can investigate them
slackrepo build --repo=csb --debug colord
```

```
# Remove the package grass
# (dependencies and dependers will not be removed)
slackpkg remove grass
```


## Commands

Slackrepo has five main commands: build, rebuild, update, remove and revert.
Other commands include lint, install and info.

#### 1. build

<pre>slackrepo build [--options] [item...]</pre>

Each specified item in the SlackBuild repository is
built and stored into the package repository if there is no up-to-date
package already stored.  Missing or out-of-date dependencies are also
built and stored into the package repository.  If no items are specified,
everything in the SlackBuild repository is built and stored.

Items can be specified as the name of an individual item (e.g. 'wxPython')
or as an individual item in a category (e.g. 'libraries/wxPython'), or as
an entire category of items (e.g. 'libraries').  Obviously, very large
amounts of storage will be needed if you build entire categories.

#### 2. rebuild

<pre>slackrepo rebuild [--options] [item...]</pre>

The rebuild command is like the build command, except that packages already in the package
repository will be rebuilt, even if they are up-to-date. That is the only difference!

#### 3. update

<pre>slackrepo update [--options] [item...]</pre>

The update command compares each specified package (and its dependencies) in the
package repository to the SlackBuild repository. Packages are built,
rebuilt or removed, as appropriate.  If no items are specified,
everything in the package repository is compared.

#### 4. remove

<pre>slackrepo remove [--options] item...</pre>

The remove command removes packages for each specified item from the package repository.
Also, any cached source files are removed. Backups are made, in case you change your mind later.

#### 5. revert

<pre>slackrepo revert [--options] item...</pre>

The revert command restores packages (and source files) for each specified item from the backup.
The existing packages are backed up, in case you change your mind again and again and again...

#### Other commands

<pre>slackrepo lint [--options] [item...]
slackrepo install [--options] [item...]
slackrepo info [--options]</pre>

  * The lint command performs quality checks on the source download URLs and source files, the SlackBuild,
    and existing packages for each specified item.

  * The install command is a quick way of typing <code>build --install</code>
    (the <code>--install</code> control argument is described below).

  * The info command prints information about your system and repository setup.

## Options

#### \-\-repo=ID

Use the SlackBuild and package repository identified by ID.  The location of
the SlackBuild and package repositories are determined by the configuration
file /etc/slackrepo/repo_ID.conf.  The default repository ID is <strong>SBo</strong>
(<a class="ext" href="https://SlackBuilds.org">SlackBuilds.org</a>), but others
supplied include
<strong>msb</strong> (<a class="ext" href="https://github.com/mateslackbuilds/msb">Mate SlackBuilds</a>) and
<strong>csb</strong> (<a class="ext" href="https://github.com/willysr/csb">Cinnamon SlackBuilds</a>).
On Slackware -current, the default repository is <strong>ponce</strong>
(<a class="ext" href="https://github.com/Ponce/slackbuilds">SBo repository for slackware-current</a>).

#### -v, \-\-verbose

Display detailed build output on the console during execution.
See <a href="#logging">Logging</a> (below) for details.

#### \-\-install

Built, rebuilt and updated packages will be installed on the host system.
This results in a "queue" style build, and packages may not be 100% clean of
unexpected dependencies. The default is for packages <strong>not</strong> to
be installed.

#### \-\-preview

Packages will not be built or removed.  Prints what would be added, updated, rebuilt
and removed, with an estimate of the total build time.

#### \-\-dry-run

Packages will be built, but will not be saved in the repository.

#### \-\-debug

This is short for <code>-vv --lint --dry-run --keep-tmp</code>. Verbose build output
will be forced. The SlackBuild files, source downloads and packages will be subjected
to quality checks. Packages will not be saved in the repository. Temporary files
will be kept so you can investigate any problems.


## Logging

All messages from slackrepo are sent to a main log file.  Progress messages are
displayed on the screen.

Detailed output from each build process is sent to an item-specific log
file, but is not displayed on the screen unless you specify the <code>--verbose</code>
(<code>-v</code>) control argument.

The log files are in a repository-specific directory.  A new main log file is created
for each run of slackrepo, with a name of the form
<br><code>/var/log/slackrepo/SBo/slackrepo_YYYY-MM-DD_hh:mm:ss.log</code>

Each item-specific log file is in a subdirectory if the SlackBuilds directory
has subdirectories.  For example, the log file for building the SBo item 'graphics/shotwell' is
<br><code>/var/log/slackrepo/SBo/graphics/shotwell/build.log</code>

The log files include
  * ChangeLog (a cumulative log of successful builds and rebuilds)
  * build.log (output from the build command)
  * build.1.log.gz (a compressed log of the previous build)
  * rebuild.log and rebuild.1.log.gz (output from the rebuild command)
  * config.log (a copy is kept here if an autotools build creates config.log)
  * install.log (if you use the install command or --install)
  * lint.log (if you use the lint command or --lint)
  * resource.log (resource usage during the most recent build)
