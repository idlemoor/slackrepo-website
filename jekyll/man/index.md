---
layout: default
title: manpages
permalink: /man/
addnav: nav-manpages.html
---

# manpages

<p>
    <a style="font-size:large" class="site-nav-link" href="{{ site.baseurl }}/man/slackrepo.8.html">slackrepo&nbsp;(8)</a>
    <br />The slackrepo command
</p>

<p>
    <a style="font-size:large" class="site-nav-link" href="{{ site.baseurl }}/man/slackrepo.conf.5.html">slackrepo.conf&nbsp;(5)</a>
    <br />Configuration file format and contents
</p>

<p>
    <a style="font-size:large" class="site-nav-link" href="{{ site.baseurl }}/man/slackrepo.hint.5.html">slackrepo.hint&nbsp;(5)</a>
    <br />Hint file format and contents
</p>
