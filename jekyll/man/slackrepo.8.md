---
layout: default
title: slackrepo (8)
unindexed: yes
addnav: nav-manpages.html
---

<h1>{{ page.title }}</h1>
<div class="manpage">
{% include man-slackrepo.8.html %}
</div>
