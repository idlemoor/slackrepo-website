---
layout: default
title: slackrepo.hint (5)
unindexed: yes
addnav: nav-manpages.html
---

<h1>{{ page.title }}</h1>
<div class="manpage">
{% include man-slackrepo.hint.5.html %}
</div>
