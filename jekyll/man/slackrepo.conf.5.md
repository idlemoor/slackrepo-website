---
layout: default
title: slackrepo.conf (5)
unindexed: yes
addnav: nav-manpages.html
---

<h1>{{ page.title }}</h1>
<div class="manpage">
{% include man-slackrepo.conf.5.html %}
</div>
