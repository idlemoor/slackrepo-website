---
layout: default
title: Screenshots
---

# Screenshots

<div style="line-height:1">

<p>
    Building the package 'development/scons' from SlackBuilds.org is
    simple. Just type
    <br /><code>slackrepo build scons</code>
</p>
<p>
    When it has finished, slackrepo shows a summary of what was done.
</p>
<p style="margin-bottom:3rem">
    <img src="{{ site.baseurl }}/screenshots/screenbuild.png" alt="screenshot of 'slackrepo build scons'" title="slackrepo build scons">
</p>



<p>
    Dependencies are automatically built.
</p>
<p>
    In this case, we want to build viking. We already have packages for
    scons and libgexiv2, but gpsd needs to be added.
</p>
<p style="margin-bottom:3rem">
    <img src="{{ site.baseurl }}/screenshots/screendeps.png" alt="screenshot of a build with dependencies: 'slackrepo build viking'" title="slackrepo build viking">
</p>

<p></p>

<p>
    When your existing packages in your package repository have been
    updated in the upstream SlackBuild repository,
    <br><code>slackrepo update</code>
    <br>will apply the updates.
</p>
<p style="margin-bottom:3rem">
    <img src="{{ site.baseurl }}/screenshots/screenupdate.png" alt="screenshot of an update: 'slackrepo update netcdf'" title="slackrepo update netcdf">
</p>

<p></p>

<p>
    A package will automatically be rebuilt if any of its dependencies
    have been updated.
</p>
<p style="margin-bottom:3rem">
    <img src="{{ site.baseurl }}/screenshots/screenrebuild.png" alt="screenshot of updated dependency and rebuild: 'slackrepo update gdal'" title="slackrepo update gdal">
</p>

<p></p>

<p>
    When a build fails, slackrepo attempts to show significant errors
    from the item's log file, using an awesome regular expression
    devised by Eric Hameleers for his
    <a class="ext" href="http://www.slackware.com/~alien/tools/checkpkg">checkpkg</a>
    script.
</p>
<p>
    The selected errors are not always complete or relevant, but they
    will usually help to locate the problem.  The item's log file will
    always contain full details.
</p>
<p style="margin-bottom:3rem">
    <img src="{{ site.baseurl }}/screenshots/screenfail.png" alt="screenshot of a build failure: 'slackrepo build xonclock'" title="slackrepo build xonclock">
</p>

<p></p>

<p>
    Use -v (--verbose) if you want to see detailed build output (like
    manual building, sbopkg, etc).
</p>
<p>
    Build output (from cmake, git, gcc, clang, mono, waf and others) is
    coloured (but if you don't like that, you can disable it). Log file
    output is filtered to remove the colour codes.
</p>
<p>
    You can make -v the default by editing the config file, setting
    <code>VERBOSE="y"</code>.
</p>
<p style="margin-bottom:3rem">
    <img src="{{ site.baseurl }}/screenshots/screenverbose.png" alt="screenshot of verbose output: 'slackrepo build -v jhead'" title="slackrepo build -v jhead">
</p>

</div>
