---
layout: default
title: News
---

# News

## slackrepo-0.3, September 2017

The following new features and improvements have been added since the
release of slackrepo-0.2.0. Thanks to Jeremy&nbsp;H., Andrzej&nbsp;T., Skaendo,
Alexander&nbsp;V., Andrew&nbsp;C. and Didier&nbsp;S. for their contributions.

  * <b>Default hintfiles have been moved to the <i>slackrepo-hints</i> package</b>,
    so you now need both <i>slackrepo</i> and <i>slackrepo-hints</i> if you use the
    default hintfiles (recommended).
  * CPU, memory and disk space usage is recorded during builds. A summary is shown
    on the console if the build takes more than five minutes, and a log is created
    in the log directory so you can investigate if you think a build failed because
    it ran out of memory or disk space.
  * The new <code>--preview</code> control argument shows you what would be added,
    updated, rebuilt or removed, and gives you an estimate of the build time,
    but does not build or remove anything.
  * The new <code>--debug</code> control argument is a quick way of setting
    <code>--very-verbose --dry-run --lint --keep-tmp</code>
  * The new <code>HOOK_START</code> and <code>HOOK_FINISH</code> configuration
    variables allow you to specify additional scripts to run before slackrepo has
    started processing, or after it has finished.
  * The new <code>SUBSTITUTE</code> configuration variable allows you to set a global
    policy for replacing dependencies in all builds, for example:
      <br /><code>SUBSTITUTE="jdk => openjdk, jack_audio_connection_kit => jack2"</code>
  * The new <code>DELREQUIRES</code> hint deletes dependencies (it's the opposite of <code>ADDREQUIRES</code>).
  * The new <code>BUILDTIME</code> hint excludes dependencies from package manager metadata after building.
  * The new 'python3' pragma gives you a single easy way to force python3 support,
    including SlackBuilds that automatically detect Python 3, SlackBuilds that need
    a <code>PYTHON3</code> option, and even to add Python 3 support into SlackBuilds that do not
    support it.
  * The new 'kernelmod' and 'kernel' pragmas indicate that a package needs to be rebuilt
    when the build system's kernel is upgraded.
  * The new 'x86arch' pragma sets the correct arch for 32 bit binary repack SlackBuilds.
  * The new 'curl' and 'wget' pragmas select which command should be used for downloading.
  * If downloading from the normal URL fails, downloads now automatically fall back to both
    <a class="ext" href="http://slackware.uk/sbosrcarch/">sbosrcarch</a> and
    <a class="ext" href="https://sourceforge.net/projects/slackbuildsdirectlinks/">Slackbuilds Direct Links</a>.
  * The <code>lint</code> command and <code>--lint</code> control argument now accept suboptions for doing checks selectively.
  * Some warnings have been revised and new warnings have been added, see <a href="{{ site.baseurl }}/doc/checks.html">Lint checks</a> for details.
  * Warnings can be suppressed selectively for all builds with the new 'NOWARNING' configuration variable.
  * Warnings can be suppressed selectively for specific builds with the new 'NOWARNING' hint.
  * Experimental support for reproducible builds has been added (set the configuration variable
    <code>REPRODUCIBLE="y"<code>).

<img src="{{ site.baseurl }}/img/xkcd.png"
     alt="xkcd1319 in practice" title="xkcd1319 in practice"
     style="display:block;" class="centred" >

## slackrepo-0.2.0, May 2015

  * Building in a dynamically created and destroyed chroot environment
    to keep your system absolutely clean, to detect bad builds and to speed up package uninstalls
    (this feature requires Linux kernel version 3.18 or newer with overlayfs)
  * Building as a non-root user to detect bad builds and keep your system clean
    (requires the 'fakeroot' package, available from SlackBuilds.org) &mdash; see the <a href="{{ site.baseurl }}/doc/faq.html">FAQ</a>
  * Automatic download fallback to <a class="ext" href="https://sourceforge.net/projects/slackbuildsdirectlinks/">Slackbuilds Direct Links</a>
    if downloading from the normal URL fails
  * On Slackware -current, the default is to build from Matteo Bernardini's forked
    <a class="ext" href="https://github.com/Ponce/slackbuilds">SBo repository for slackware-current</a> (\-\-repo=ponce)
  * Build time estimates are shown, using a database of known build times,
    or the last occasion you successfully built the same package
  * Automatic package backup is performed, so you can go back to the
    previous build of a package
  * New 'lint' command allows you to check or recheck SlackBuilds,
    source download links and package contents without building or rebuilding
  * Coloured output from cmake, git, clang, mono, waf and gcc-4.9
    when you build with \-\-verbose
  * Shell-style globs (wildcards) are now supported in the command arguments, so you can do things like
    <br><code>slackrepo build 'xfce4-*-plugin'</code>
    <br>(As with commands like 'find', you will need to quote them or escape them,
    to protect them from the shell.)
  * System-supplied hintfiles have moved to the directory
    <br><code>/etc/slackrepo/<i>repo</i>/default_hintfiles/<i>slackversion</i></code>
    <br>and are now enabled by default. Your own local hintfiles override the
    default hintfiles, and are still read from the directory
    <code>/etc/slackrepo/<i>repo</i>/hintfiles</code>.
    You can disable the default hintfiles by disabling the DEFAULT_HINTDIR configuration variable.
  * The SPECIAL hint has been renamed to PRAGMA (but SPECIAL is still accepted)
  * Revised and additional hintfiles Thanks to Panagiotis Nikolaou for the contributions!
  * sqlite database to track package revision info
    (the previous release of slackrepo used dotfiles)
  * Simplified \-\-verbose option, \-\-quiet option removed
