---
layout: default
title: Slackware package management links
addnav: nav-doc.html
---

# Slackware package management links

## Background information

  * slackware.com &mdash; <a class="ext" href="http://www.slackware.com/config/packages.php">pkgtool, installpkg, upgradepkg, removepkg etc.</a>
  * Slackware Documentation Project &mdash; <a class="ext" href="https://docs.slackware.com/howtos:misc:get_acquainted_with_slackware#management_of_software_packages">Management of software packages</a>
  * Slackware Documentation Project &mdash; <a class="ext" href="https://docs.slackware.com/howtos:slackware_admin:how_to_use_slackware_installing_software">Installing Software</a>
  * Slackbook Chapter 18 &mdash; <a class="ext" href="http://www.slackbook.org/html/package-management.html">Slackware Package Management</a>


## Other package management tools

  * <a class="ext" href="https://www.slackpkg.org/">slackpkg</a> (distributed in Slackware ap/) is a tool for installing or upgrading packages through a network.
  * <a class="ext" href="http://slakfinder.org/slackpkg+.html">slackpkg+</a> is a plugin for slackpkg. It adds support for third-party repositories.
  * <a class="ext" href="https://www.sbopkg.org/">sbopkg</a> is a command-line and dialog-based tool for SlackBuilds.org.
    <br />sqg (distributed in sbopkg) generates sbopkg queuefiles.
  * <a class="ext" href="https://pink-mist.github.io/sbotools/">sbotools</a> provides a ports-like interface to SlackBuilds.org.
  * <a class="ext" href="https://github.com/dslackw/slpkg">slpkg</a> is a utility to help package management in Slackware.
  * <a class="ext" href="https://github.com/aadityabagga/asbt">asbt</a> is a tool for managing packages in your local copy of SlackBuilds.org.
  * <a class="ext" href="https://github.com/nihilismus/sbo_tools">sbo_tools</a> is a set of Bash scripts to help you in your daily use of SlackBuilds.org.
  * <a class="ext" href="https://github.com/cwilling/hoorex">hoorex</a> determines (based on REQUIRES field of SBo .info files) Who Requires a particular package.
  * <a class="ext" href="https://github.com/gapan/slkbuild">slkbuild</a> is an Arch-like wrapper script for easy Slackware packaging.
  * <a class="ext" href="http://www.slackware.com/~alien/tools/gen_repos_files.sh">gen_repos_files.sh</a> maintains metadata in a package repository, including package signing and a changelog.
  * <a class="ext" href="http://www.slackware.com/~alien/tools/checkpkg">checkpkg</a> is a script for checking a Slackware package or a build log for defects and irregularities.
  * <a class="ext" href="http://www.slackware.com/~mozes/">slacktrack</a> (distributed in Slackware d/) is a tool to assist with building Slackware packages from &lt;package&gt;.build scripts.
  * <a class="ext" href="https://alien.slackbook.org/AST/">Alien's SlackBuild Toolkit</a> is a web-based SlackBuild generator.
  * <a class="ext" href="http://dawoodfall.net/slackbuilds/noversion/mkslack">mkslack</a> is a SlackBuild generator script.
  * <a class="ext" href="https://qtgzmanager.wordpress.com/">QTGZManager</a> is a Slackware pkgtools GUI and frontend.
  * <a class="ext" href="https://github.com/montagdude/sboui">sboui</a> is an ncurses-based user interface for sbopkg and sbotools.
  * <a class="ext" href="http://rg3.github.io/slackroll/">Slackroll</a> is a package or update manager for Slackware systems.
  * <a class="ext" href="http://sourceforge.net/p/usm/wiki/Home/">usm</a> is a unified slackware package manager that supports dependency resolution.
  * <a class="ext" href="https://github.com/vbatts/slack-utils">slack-utils</a> is a handful of commands to quickly/easily access information on the Slackware Linux distribution.
  * <a class="ext" href="http://depfinder.sourceforge.net/">depfinder</a> scans Slackware packages and outputs a list of their dependencies.
  * <a class="ext" href="https://bitbucket.org/a4z/sbbdep/wiki/Home">sbbdep</a> is a tool for exploring binary runtime dependencies on Slackware and Slackware based systems.
  * <a class="ext" href="https://slackbuilds.org/repository/14.2/development/cpan2tgz/">cpan2tgz</a> (available via SBo) creates Slackware packages from the CPAN repository.
  * <a class="ext" href="https://slackbuilds.org/repository/14.2/development/gem2tgz/">gem2tgz</a> (available via SBo) creates Slackware packages from the rubygem repository.
  * <a class="ext" href="https://slackbuilds.org/repository/14.2/development/pip2tgz/">piptgz</a> (available via SBo) creates Slackware packages from the PyPi repository.
  * <a class="ext" href="https://slackbuilds.org/repository/14.2/development/npm2tgz/">npm2tgz</a> (available via SBo) creates Slackware packages from the npm repository.

### Other distros

  * <a class="ext" href="https://www.pkgsrc.org/">pkgsrc</a> is a framework for building third-party software on NetBSD and other UNIX-like systems.
  * <a class="ext" href="https://www.salixos.org/">Salix</a> tools include
    <a class="ext" href="https://github.com/gapan/spi">spi</a>,
    <a class="ext" href="https://github.com/gapan/sbobuild">sbobuild</a>,
    <a class="ext" href="https://software.jaos.org/">slapt-get and gslapt</a>,
    <a class="ext" href="https://github.com/gapan/sourcery">sourcery</a>,
    and <a class="ext" href="https://github.com/gapan/spkg">spkg</a>.
  * <a class="ext" href="http://zenwalk.org/">Zenwalk GNU Linux</a> has <a href="http://manual.zenwalk.org/en/Package%20Management.html">netpkg</a>.
