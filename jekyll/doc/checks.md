---
layout: default
title: Lint checks
addnav: nav-doc.html
---

# Lint checks

These checks are performed by the <code>lint</code> command, and by the
<code>build</code>, <code>rebuild</code> and <code>update</code>
commands if the <code>--lint</code> control argument is specified.
When any checks fail, warnings are displayed and logged, but building continues.

There are six groups of checks that can be specified by the LINT configuration
variable or the --lint control argument:

  * <b>sb</b> for the SlackBuild files,
  * <b>dl</b> for the download URLs,
  * <b>x</b> to prevent access to the X GUI during the build,
  * <b>net</b> to prevent access to the network during the build,
  * <b>pkg</b> to check the package name, format and contents, and
  * <b>inst</b> to temporarily install the package

### 'sb' check

##### prgnam.SlackBuild

  * Should exist

##### slack-desc

  * Should exist
  * Should have between 1 and 14 lines that start with <code>prgnam:</code>
  * If the handy-ruler exists, it must be aligned correctly and have the correct format
  * No trailing spaces on <code>prgnam:</code> lines, i.e. no lines should match <code>^prgnam:&lt;sp&gt;&lt;sp&gt;*$</code>
  * No <code>prgnam:</code> lines should have more than 73 chars of description
  * All other lines must be prefixed by <code>#</code>, or be completely empty

##### prgnam.info

  * Optional; its existence indicates an SBo-style SlackBuild
  * Should define PRGNAM, VERSION, HOMEPAGE, DOWNLOAD(_arch), MD5SUM(_arch), REQUIRES, MAINTAINER, EMAIL
  * PRGNAM should be the same as the .info file's name and the .SlackBuild file's name
  * Each item defined in REQUIRES should exist in the repo

##### README

  * Optional; if prgnam.info exists, README should exist
  * Should not contain long lines &gt;= 80 chars

### 'dl' check

  * Download URLs should work; to check this, the header is retrieved
  * If the file is already in the source repository, the file size reported in the
    header should be the same as the size of the file in the source repository

### 'x' check

  * The package should build with no X display

### 'net' check

  * The package should not download anything or perform tests that need the network

### 'pkg' check

##### Package name

  * Package name should be in form $PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE
  * If the slackbuild produces one package, PRGNAM should be the same as the slackbuild's name and should be the same as PRGNAM in the .info file
  * VERSION should be the same as VERSION in the .info file
  * ARCH should be i?86 or x86_64 (optionally suffixed with _<i>KNLVER</i>, _<i>LOCALE</i> etc), or noarch
  * BUILD should be numeric
  * TAG should be same as the repo's TAG
  * PKGTYPE should be tgz, txz, tbz or tlz (and should be as set by the caller)

##### Package contents

  * Should be tar-1.13 compatible (path prefix should not start with <code>./</code>)
  * Compression type should correspond to package type suffix
  * Should only install to (bin)\|(boot)\|(dev)\|(etc)\|(lib)\|(opt)\|(sbin)\|(srv)\|(usr)\|(var)\|(install)
  * should contain install/slack-desc
  * nothing installed to usr/local
  * nothing installed to usr/share/man
  * all manpages gz compressed
  * should install without error

### 'inst' check

  * installpkg should run without returning an error status or printing an error message
