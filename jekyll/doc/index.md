---
layout: default
title: Documentation
permalink: /doc/
addnav: nav-doc.html
---

# Documentation

<p>
    <a style="font-size:large" class="site-nav-link" href="{{ site.baseurl }}/doc/usage.html">Usage&nbsp;notes</a>
    <br />New users start here
    <br />
      <a href="{{ site.baseurl }}/doc/usage.html#dependencies">Dependencies</a>
      &bullet;&nbsp;<a href="{{ site.baseurl }}/doc/usage.html#git">git </a>
      &bullet;&nbsp;<a href="{{ site.baseurl }}/doc/usage.html#updates">Updates</a>
      &bullet;&nbsp;<a href="{{ site.baseurl }}/doc/usage.html#gen_repos_filessh">gen_repos_files.sh</a>
      &bullet;&nbsp;<a href="{{ site.baseurl }}/doc/usage.html#slackpkg">slackpkg+</a>
</p>

<p>
    <a style="font-size:large" class="site-nav-link" href="{{ site.baseurl }}/doc/faq.html">FAQ</a>
    <br />Frequently asked questions
</p>

<p>
    <a style="font-size:large" class="site-nav-link" href="{{ site.baseurl }}/doc/config.html">Configuration</a>
    <br />Format and contents of slackrepo_SBo.conf etc
</p>

<p>
    <a style="font-size:large" class="site-nav-link" href="{{ site.baseurl }}/doc/hintfiles.html">Hintfiles</a>
    <br />Files that specify individual build options
</p>

<p>
    <a style="font-size:large" class="site-nav-link" href="{{ site.baseurl }}/doc/structure.html">Repository&nbsp;structure</a>
    <br />Layout and structure of the package respository
</p>

<p>
    <a style="font-size:large" class="site-nav-link" href="{{ site.baseurl }}/doc/checks.html">Lint checks</a>
    <br />Description of optional checks
</p>

<p>
    <a style="font-size:large" class="site-nav-link" href="{{ site.baseurl }}/doc/links.html">Links</a>
    <br />External resources
    <br />
      <a href="{{ site.baseurl }}/doc/links.html#background-information">Background&nbsp;information</a>
      &bullet;&nbsp;<a href="{{ site.baseurl }}/doc/links.html#other-package-management-tools">Other&nbsp;package&nbsp;management&nbsp;tools</a>
</p>

<p>
    <a style="font-size:large" class="site-nav-link" href="{{ site.baseurl }}/man/">manpages</a>
    <br />
        <a href="{{ site.baseurl }}/man/slackrepo.8.html">slackrepo(8)</a>
        &bullet;&nbsp;<a href="{{ site.baseurl }}/man/slackrepo.conf.5.html">slackrepo.conf(5)</a>
        &bullet;&nbsp;<a href="{{ site.baseurl }}/man/slackrepo.hint.5.html">slackrepo.hint(5)</a>
</p>
