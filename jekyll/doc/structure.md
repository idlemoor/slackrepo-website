---
layout: default
title: Repository structure
addnav: nav-doc.html
---

# Repository structure

## Repository paths

Multiple repositories are supported.  The Repository ID, e.g. <b>SBo</b>,
selects a configuration file, e.g. <code>/etc/slackrepo/slackrepo_<b>SBo</b>.conf</code>, and
the configuration file sets the paths of the SlackBuild repository, the
source repository, the package repository, the hintfile and queuefile directories, and the
log directory.  The Repository ID can be specified by the control argument
<code>--repo=ID</code>, or by the <code>REPO</code> environment variable, or by setting <code>REPO=ID</code>
in the file <code>~/.slackreporc</code>.  By default, the default default is <b>SBo</b>.

Example showing repository trees for Repository ID <b>SBo</b>:

```
[REPO=SBo] /etc/slackrepo/slackrepo_SBo.conf
            |
[SBREPO]    |--/var/lib/slackrepo/SBo/slackbuilds/category/prgnam/prgnam.SlackBuild
[SRCREPO]   |--/var/lib/slackrepo/SBo/source/category/prgnam/prgnam-1.4.tar.gz
[PKGREPO]   |--/var/lib/slackrepo/SBo/packages/14.2/i586/category/prgnam/prgnam-1.4-i586-1_SBo.tgz
[PKGBACKUP] |--/var/lib/slackrepo/SBo/backups/14.2/i586/category/prgnam/prgnam-1.3-i586-1_SBo.tgz
            |
[DATABASE]  |--/var/lib/slackrepo/SBo/database_SBo_i586.sqlite3
            |
[HINTDIR]   |--/etc/slackrepo/SBo/hintfiles/[category/prgnam/]prgnam.hint
            |
[LOGDIR]    |--/var/log/slackrepo/SBo/category/prgnam/build.log
            |--/var/log/slackrepo/SBo/category/prgnam/build.1.log.gz
            |--/var/log/slackrepo/SBo/category/prgnam/config.log
```

Under each of SBREPO, SRCREPO, PKGREPO, PKGBACKUP and LOGDIR is a subdirectory structure for each
SBo category and item. In HINTDIR, the subdirectory structure is optional.

## Package repository files

The following files are created by gen_repos_files.sh and slackrepo in the package repository.

In the package repository's root directory:

  * <code>CHECKSUMS.md5</code> - md5 checksums for all the packages and the package metadata
  * <code>CHECKSUMS.md5.asc</code> - a detached GPG signature that can be used to verify CHECKSUMS.md5
  * <code>CHECKSUMS.md5.gz</code> - a compressed copy of CHECKSUMS.md5
  * <code>CHECKSUMS.md5.gz.asc</code> - a detached GPG signature that can be used to verify CHECKSUMS.md5.gz
  * <code>ChangeLog.rss</code> - an RSS feed of recent ChangeLog.txt entries
  * <code>ChangeLog.txt</code> - the cumulative ChangeLog for this package repository
  * <code>ChangeLog.txt.gz</code> - a compressed copy of ChangeLog.txt
  * <code>FILELIST.TXT</code> - a listing of all the files in the package repository
  * <code>GPG-KEY</code> - your public GPG key
  * <code>MANIFEST.bz2</code> - a compressed listing of the contents of all the packages in the package repository
  * <code>PACKAGES.TXT</code> - a listing of all the packages in the package repository
  * <code>PACKAGES.TXT.gz</code> - a compressed copy of PACKAGES.TXT

For each package:

  * <code>package.tgz</code> (or .txz, .tbz, .tlz) - the package itself
  * <code>package.tgz.asc</code> (or .txz.asc, .tbz.asc, .tlz.asc) - a detached GPG signature that can be used to verify the package
  * <code>package.tgz.md5</code> (or .txz.md5, .tbz.md5, .tlz.md5) - the package's md5sum
  * <code>package.dep</code> - a plaintext list of all dependencies of the package (including indirect dependencies, but excluding any dependencies in Slackware itself)
  * <code>package.lst</code> - a listing of the contents of the package
  * <code>package.meta</code> - package management information, for generating PACKAGES.TXT
  * <code>package.txt</code> - text description of the package (based on the slack-desc)
  * <code>package.buildinfo</code> - (if enabled) experimental reproducible build information
