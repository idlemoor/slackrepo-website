---
layout: default
title: Configuration
addnav: nav-doc.html
---

# Configuration

These configuration options are set in <code>slackrepo_SBo.conf</code>
or <code>~/.slackreporc</code>.

<i>For full details, see the manpage
<a href="{{ site.baseurl }}/man/slackrepo.conf.5.html">slackrepo.conf(5)</a></i>


### Default repo ID and configuration directory

  * <code>REPO="SBo"</code> &mdash; the default repo ID.  This determines the configuration file
    <code>slackrepo_<i>ID</i>.conf</code> from which other configuration variables will be read.
    This can be set <b>only</b> in <code>~/.slackreporc</code>, but can be overridden by the <code>REPO</code> environment
    variable or by the command line option <code>--repo=<i>ID</i></code>.
  * <code>CONFIGDIR="/etc/slackrepo"</code> &mdash; the configuration directory.  This specifies an alternative
    directory to be used instead of <code>/etc/slackrepo</code> to find the configuration files.
    This can be set <b>only</b> in <code>~/.slackreporc</code>, but can be overridden by the <code>CONFIGDIR</code> environment
    variable.


### Repository filestore locations

The strings <code>%REPO%</code>, <code>%SLACKVER%</code> and <code>%ARCH%</code>
are replaced by the repository ID (e.g. SBo), the Slackware version (e.g. 14.2),
and the Slackware ARCH (e.g. i586).

  * <code>SBREPO="/var/lib/slackrepo/%REPO%/slackbuilds"</code> &mdash; location of SlackBuilds repository
  * <code>SRCREPO="/var/lib/slackrepo/%REPO%/source"</code> &mdash; location of downloaded source repository
  * <code>PKGREPO="/var/lib/slackrepo/%REPO%/packages/%SLACKVER%/%ARCH%"</code> &mdash; location of package repository
  * <code>PKGBACKUP="/var/lib/slackrepo/%REPO%/backups/%SLACKVER%/%ARCH%"</code> &mdash; location of backups
  * <code>HINTDIR="/etc/slackrepo/%REPO%/hintfiles"</code> &mdash; location of locally written <a href="{{ site.baseurl }}/doc/hintfiles.html">Hintfiles</a>
  * <code>DEFAULT_HINTDIR="/etc/slackrepo/%REPO%/default_hintfiles/%SLACKVER%"</code> &mdash; location of system-provided default hintfiles
    (these come in a separate <a href="{{ site.baseurl }}/download.html">slackrepo-hints package</a>)
  * <code>LOGDIR="/var/log/slackrepo/%REPO%"</code> &mdash; location of log files
  * <code>DATABASE="/var/lib/slackrepo/%REPO%/database_%REPO%_%ARCH%.sqlite3"</code> &mdash; location of the database
  * <code>TMP="/tmp/%REPO%"</code> &mdash; directory for temporary files

If you want to run slackrepo as an ordinary user, you can use something like
this instead, and save it to .slackreporc in your ordinary user's home directory:
```
SBREPO=~/slackrepo/%REPO%/slackbuilds
SRCREPO=~/slackrepo/%REPO%/source
PKGREPO=~/slackrepo/%REPO%/packages/%SLACKVER%/%ARCH%
PKGBACKUP=~/slackrepo/%REPO%/backups/%SLACKVER%/%ARCH%
HINTDIR=~/slackrepo/%REPO%/hintfiles
DEFAULT_HINTDIR="/etc/slackrepo/%REPO%/default_hintfiles/%SLACKVER%"
LOGDIR=~/slackrepo/%REPO%/logs
DATABASE=~/slackrepo/%REPO%/database_%REPO%_%ARCH%.sqlite3
```
If you use <code>~</code> you should NOT use quotes <code>"..."</code>. You also need to edit the file
<code>/etc/sudoers.d/slackrepo</code>, replacing <code><i>username</i></code> with your
username:
<pre>
User_Alias SLACKREPOERS = <i>username</i>
Defaults:SLACKREPOERS !env_reset,!env_delete
SLACKREPOERS ALL=(ALL) NOPASSWD: /usr/libexec/slackrepo/*
</pre>


### Building packages

  * <code>SUBSTITUTE=""</code> &mdash; a list of dependency substitutions applied to
    all SlackBuilds.  To substitute jdk with openjdk, and jack-audio-connection-kit with jack2:
    <br><code>SUBSTITUTE="jdk => openjdk, jack-audio-connection-kit => jack2"</code>
  * <code>NUMJOBS=""</code> &mdash; number of make jobs to set in MAKEFLAGS (e.g., <code>NUMJOBS="-j2"</code>).
    Leave blank to have this dynamically controlled according to the number of processors on the build host
    and the current load average.
  * <code>ARCH=""</code> &mdash; arch for built packages (normally determined from the build host)
  * <code>TAG="_SBo"</code> &mdash; tag for built packages (<strong>please change this</strong>
    if your packages will be publicly available, see <a class="ext" href="https://slackbuilds.org/faq/#package_repos">SlackBuilds.org FAQ 20</a>)
  * <code>PKGTYPE="txz"</code> &mdash; package compression type. Valid values are: tgz, txz, tbz, tlz


### Calling gen_repos_files.sh

  * <code>USE_GENREPOS="0"</code> &mdash; whether to use gen_repos_files.sh (to enable it, change 0 to 1)

If you enable gen_repos_files.sh, you <strong>must</strong> set correct values for its
configuration options in <code>/etc/slackrepo/slackrepo_SBo.conf</code>.  However, if
you already use gen_repos_files.sh, it will still read your existing
<code>~/.genreprc</code> file.  For details, see the man page <a href="{{ site.baseurl }}/man/slackrepo.conf.5.html">slackrepo.conf(5)</a>.
