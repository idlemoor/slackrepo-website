---
layout: default
title: Hintfiles
addnav: nav-doc.html
---

# Hintfiles

You are encouraged to take full control of the building process by
reviewing, customising and creating <strong><em>hintfiles</em></strong>.
A hintfile is a file that supplements a SlackBuild's
.info file, for example to specify options and additional dependencies for the
SlackBuild, to override the version, downloads and md5sums, etc.

The format of a hintfile is based on the format of the SlackBuilds.org .info file,
but all the variables are <strong>optional</strong>, and any unrecognised variables are
<strong>ignored</strong>. Most SlackBuilds do not need hintfiles, and most
hintfiles will only have one or two variables. The variables you can set are as follows:

<pre>
OPTIONS="OPT1=yes OPT2=no ..."
ADDREQUIRES="additional dependencies"
DELREQUIRES="dependencies to ignore"
BUILDTIME="dependencies to exclude from package manager info"
GROUPADD="gid:groupname ..."
USERADD="uid:username[:controlargs...] ..."
VERSION="newversion"
DOWNLOAD="new urls"
MD5SUM="new md5sums"
SHA256SUM="sha256sum"
DOWNLOAD_<i>arch</i>="new urls"
MD5SUM_<i>arch</i>="new md5sums"
SHA256SUM_<i>arch</i>="sha256sum"
SKIP="yesno"
ARCH="arch"
INSTALL="yesno"
NUMJOBS="-j1"
NOWARNING="Extended RE"
ANSWERS="strings"
CLEANUP="commands"
PRAGMA="pragmas"
</pre>

Each repository has two directories for hintfiles: one for your own local hintfiles,
and one for system-supplied default hintfiles. For the SBo repository, your own directory is
<code>/etc/slackrepo/SBo/hintfiles</code>
and the default hintfile directory is
<code>/etc/slackrepo/SBo/default_hintfiles/14.2</code>

<p class="scary">
You should always review the README files of the SlackBuilds that you are building,
and then consider whether you need to create a hintfile for your own requirements
in <code>/etc/slackrepo/SBo/hintfiles/</code></p>

The default hintfile directory contains a structure of subdirectories that
is the same as the subdirectories in the SlackBuilds repository.
For example, if the SBo repository's SlackBuild file for wxPython is
<code>libraries/wxPython/wxPython.SlackBuild</code>, then its default hintfile will be
<code>/etc/slackrepo/SBo/default_hintfiles/14.2/libraries/wxPython/wxPython.hint</code>.
In your own hintfile directory, you can use the same structure, or you can just
put your own hintfiles in <code>/etc/slackrepo/SBo/hintfiles/</code>.

Your own local hintfiles override the default hintfiles.

The default hintfiles will be updated every time you upgrade the
<b>slackrepo-hints</b> package.

If you don't want to use the default hintfiles, you can disable them completely
by setting the variable DEFAULT_HINTDIR to null or commenting it out
in the configuration file, or by not installing the <b>slackrepo-hints</b> package.

### OPTIONS="OPT1=yes OPT2=no ..."

This hint should define options that are described in the SlackBuild's README file.

### ADDREQUIRES="additional dependencies"

To specify the optional requirements that are described in the SlackBuild's README file,
you should set this variable to a list of dependencies, separated by spaces.
Dependencies in this list will be added to the dependencies in
the REQUIRES= list defined in the SlackBuild's .info file.

### DELREQUIRES="dependencies to ignore"

To ignore dependencies in the REQUIRES= list, use this variable.

### BUILDTIME="dependencies to exclude from package manager metadata"

Use this hint to exclude dependencies from the package manager
dependency metadata in the package repo (that is, the .dep files and
optional FOR_SLAPTGET records).

### GROUPADD="gid:groupname ..."

This hint defines groups to be added on the host system.  If a group already exists,
nothing will be done. If more than one group is to be added, a space-separated list can be specified.
Example:
```
GROUPADD="209:postgres"
```

### USERADD="uid:username[:controlargs...] ..."

This hint defines usernames to be added on the host system.  If a username already exists,
nothing will be done. Optionally, additional control arguments of the useradd
command can be specified, separated by colons, with the space between the control argument
and its argument omitted. If more than one username is required, a space-separated list of
specifications can be defined. Examples:
```
USERADD="209:postgres:-d/var/lib/pgsql"
USERADD="270:smtpd:-g0:-r:-s/bin/false:-d/var/empty 271:smtpq:-r:-s/bin/false:-d/var/empty"
```

### VERSION="newversion"

This hint can be specified to override the version
number in both the SlackBuild and .info files.
This is done by a simple text substitution, so it is not guaranteed to work
(particularly if the old version number is something like '1'), and you might need to
set a new value for the DOWNLOAD variable in the hintfile.
The md5sum of the source archive will not be checked, unless you have also supplied a new
md5sum in the hint file. The patched SlackBuild and info files are not kept.

### DOWNLOAD="new urls"

This hint redefines download URLs that replace the URLs specified in the .info file, for
example if you have also defined VERSION=, or if the upstream URL has changed.
However, if specified as <code>DOWNLOAD="no"</code>, this hint indicates that the source needs to be downloaded
manually (for example, because of a licence agreement).  If you have already downloaded
the source into the source cache, and if the md5sum is correct, then the package will be built.
Otherwise, a message will be displayed explaining what you should do, and the package will be skipped.

### MD5SUM="new md5sums"

This hint redefines md5sums that replace the md5sums specified in the .info file, for
example if you have also redefined VERSION=, or if the upstream source archive has been changed.
However, if specified as <code>MD5SUM="no"</code>, this hint indicates that the source md5sums are not
reliable (for example, because of an unversioned archive) and should not be checked.

### SHA256SUM="sha256sums"

This hint specifies an sha256sum that will be checked (in addition to any md5sum).

### DOWNLOAD_<i>arch</i>="new urls"<br />MD5SUM_<i>arch</i>="new md5sums"<br />SHA256SUM_<i>arch</i>="sha256sums"

These hints redefine DOWNLOAD and MD5SUM, and specify SHA256SUM, as above, for the specified arch.

### SKIP="yesno"

This hint can be specified if you want to skip the package when it would
otherwise be built, rebuilt, updated or removed.  If the hint isn't specified
as <code>SKIP="yes"</code> or <code>SKIP="no"</code>, it will be interpreted
as "yes", and the string will be displayed when the build is skipped, so that
you can put a helpful message into it, for example:
```
SKIP="Please remove the Slackware nc package before you build netcat-openbsd"
```

### ARCH="arch"

This hint temporarily overrides the value of ARCH given in the
configuration file or environment.  This hint is most useful for
SlackBuilds that repackage binaries.

### INSTALL="yesno"

This hint temporarily overrides the value of INSTALL given in the
configuration file or on the command line. Use this hint if you want a specific
package to be installed after it has been built (e.g. nvidia-driver). Note that
if the control argument <code>--dry-run</code> is specified, this hint will be ignored.

### NUMJOBS="-j1"

This hint temporarily overrides the value of NUMJOBS given in the
configuration file or environment.  Use this hint if the build process fails
when multiple 'make' jobs are used, so that the build will be executed with
MAKEFLAGS="-j1".

### NOWARNING="Extended RE"

This hint suppresses slackrepo warnings that match the specified extended regular
expression. Multiple warnings can be suppressed by using a suitable expression,
for example:
```
NOWARNING="(Nonstandard directories|Unexpected owner/group)"
```

### CLEANUP="commands"

This hint can be specified if the package needs extra cleanup when it is
uninstalled (e.g. packages that replace standard Slackware packages or
install kernel modules). The hint should define an appropriate shell command pipeline.
For example, to reinstall Slackware packages:
```
CLEANUP="echo y | slackpkg -dialog=off -only_new_dotnew=on reinstall tetex tetex-doc"
```
or to clean up after a kernel module is uninstalled:
```
CLEANUP="depmod -a"
```

### ANSWERS="string"

This hint can be specified if the build process wants to read answers to
its questions during execution, for example concerning a licence agreement. The
string is piped into the SlackBuild's standard input, so it should contain whatever
will make the build process happy. Newlines can be specified as \n, for example
<code>ANSWERS="ok\nyes"</code>

### PRAGMA="pragmas"

This hint can be specified to perform one or more pragmas (i.e. special actions).
<strong>Pragmas are not guaranteed to work!</strong>
The following pragmas are available:

<b>python3</b>
&mdash; force python3 support: add python3 to the dependencies,
set the PYTHON3="yes" option, and if necessary patch the SlackBuild to support python3.

<b>kernelmodule</b> and <b>kernel</b>
&mdash; rebuild when the build host's kernel is upgraded; kernelmodule also causes
depmod be run when the package is uninstalled.

<b>multilib_ldflags</b>
&mdash; on a <a class="ext" href="http://docs.slackware.com/slackware:multilib">multilib</a> host,
attempt to patch the SlackBuild to specify LDFLAGS, as described in
<a class="ext" href="https://slackbuilds.org/faq/#multilib">SlackBuilds.org FAQ 18</a>.

<b>wget</b> or <b>curl</b>
&mdash; download with a fake wget or curl user agent string

<b>download_basename</b>
&mdash; provide a symlink for source downloads using the URL's basename,
for the benefit of SlackBuilds that don't expect content-disposition to be respected.

<b>need_net</b> and <b>need_X</b>
&mdash; do not block the network and the X display when building

<b>x86arch=<i>arch</i></b>
&mdash; when building on 32 bit x86, set the ARCH to <i>arch</i>

<b>noexport_ARCH</b> and <b>noexport_BUILD</b> and <b>noexport_TAG</b>
&mdash; set ARCH or BUILD or TAG by patching the SlackBuild instead of exporting
it to the environment.

<b>unset_ARCH</b>
&mdash; unset the value of ARCH, overriding the value given in the configuration file or environment.
Other variables can be specified, for example <code>PRAGMA="unset_DISTCC_HOSTS"</code>

<b>noremove</b>
&mdash; do not remove (uninstall) packages before building the item.
This is particularly useful for gcc, glibc, etc.

<b>nofakeroot</b>
&mdash; override building the package as a non-root user with fakeroot.
Build the package as root, using <code>sudo.</code>

<b>no_make_test</b>
&mdash; patch the SlackBuild to comment out any 'make test' command.

<b>stubs-32</b>
&mdash; temporarily provide <code>/usr/include/gnu/stubs-32.h</code>
as described in the memtest86+ README and xen SlackBuild.

<b>abstar</b>
&mdash; patch the SlackBuild to use the tar command's <code>--absolute-names</code>
control argument. Use this if you get the error message
<br><code>tar: Directory renamed before its status could be extracted</code>.
