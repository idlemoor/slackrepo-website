---
layout: default
title: Download
---

# Download

## Prebuilt packages

If you are a new user, you need to install both these packages.
They are architecture independent, suitable for i586, x86_64 or arm.

<p style="margin-bottom:0">
    <b><a class="ext" href="https://files.idlemoor.tk/packages/slackrepo-0.3-noarch-1_dbs.txz">slackrepo-0.3-noarch-1_dbs.txz</a></b>
</p>
<table style="line-height:1; border:0">
    <tr><td>version:</td><td><b><a class="ext" href="https://github.com/idlemoor/slackrepo/releases">0.3</a></b></td></tr>
    <tr><td>date:</td><td>2017-09-01</td></tr>
    <tr><td>signature:</td><td><b><a class="ext" href="https://files.idlemoor.tk/packages/slackrepo-0.3-noarch-1_dbs.txz.asc">slackrepo-0.3-noarch-1_dbs.txz.asc</a></b></td></tr>
    <tr><td>md5sum:</td><td><code>e2fa43e02c2b5e07fb98ace582695c16</code></td></tr>
    <tr><td>sha256sum:</td><td><code>b3339615a0c1c097889979dc05e57e427d1d8271e439bb494aceca1744b7b74b</code></td></tr>
    <tr><td>size:</td><td>52kB</td></tr>
</table>

<p style="margin-bottom:0">
    <b><a class="ext" href="https://files.idlemoor.tk/packages/slackrepo-hints-20170826-noarch-1_dbs.txz">slackrepo-hints-20170826-noarch-1_dbs.txz</a></b>
</p>
<table style="line-height:1; border:0">
    <tr><td>version:</td><td><b><a class="ext" href="https://github.com/idlemoor/slackrepo-hints/releases">20170826</a></b></td></tr>
    <tr><td>date:</td><td>2017-08-26</td></tr>
    <tr><td>signature:</td><td><b><a class="ext" href="https://files.idlemoor.tk/packages/slackrepo-hints-20170826-noarch-1_dbs.txz.asc">slackrepo-hints-20170826-noarch-1_dbs.txz.asc</a></b></td></tr>
    <tr><td>md5sum:</td><td><code>e2fa43e02c2b5e07fb98ace582695c16</code></td></tr>
    <tr><td>sha256sum:</td><td><code>b3339615a0c1c097889979dc05e57e427d1d8271e439bb494aceca1744b7b74b</code></td></tr>
    <tr><td>size:</td><td>148kB</td></tr>
</table>

## Git snapshot

You can clone the <a class="ext" href="https://github.com/idlemoor/slackrepo">git repository</a>, and then run the embedded
SlackBuild to create a package that you can install:

```
git clone https://github.com/idlemoor/slackrepo.git
cd slackrepo
gitrev=git$(git log -n 1 --format=format:%h .)
git archive --format=tar --prefix=slackrepo-$gitrev/ HEAD \
  | gzip > SlackBuild/slackrepo-$gitrev.tar.gz
cd SlackBuild
VERSION=$gitrev TAG=_github sh ./slackrepo.SlackBuild
upgradepkg --install-new /tmp/slackrepo-$gitrev-noarch-1_github.t?z
cd -

git clone https://github.com/idlemoor/slackrepo-hints.git
cd slackrepo-hints
gitrev=git$(git log -n 1 --format=format:%h .)
git archive --format=tar --prefix=slackrepo-hints-$gitrev/ HEAD \
  | gzip > SlackBuild/slackrepo-hints-$gitrev.tar.gz
cd SlackBuild
VERSION=$gitrev TAG=_github sh ./slackrepo.SlackBuild
upgradepkg --install-new /tmp/slackrepo-hints-$gitrev-noarch-1_github.t?z
```

# Licence information

### slackrepo

<pre class="lic">
<a class="ext" href="https://github.com/idlemoor/slackrepo">https://github.com/idlemoor/slackrepo</a>
<a class="ext" href="https://github.com/idlemoor/slackrepo-hints">https://github.com/idlemoor/slackrepo-hints</a>

Copyright &copy; 2014-2017 David Spencer, Baildon, West Yorkshire, U.K.
All rights reserved.

Redistribution and use of this software, with or without modification, is
permitted provided that the following conditions are met:

1. Redistributions of this software must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
   EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
   OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
   ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
</pre>

### gen_repos_files.sh and checkpkg

<pre class="lic">
<a class="ext" href="http://www.slackware.com/~alien/tools/gen_repos_files.sh">http://www.slackware.com/~alien/tools/gen_repos_files.sh</a>
<a class="ext" href="http://www.slackware.com/~alien/tools/checkpkg">http://www.slackware.com/~alien/tools/checkpkg</a>

Copyright &copy; 2006-2014  Eric Hameleers, Eindhoven, The Netherlands
All rights reserved.

   Permission to use, copy, modify, and distribute this software for
   any purpose with or without fee is hereby granted, provided that
   the above copyright notice and this permission notice appear in all
   copies.

   THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
   IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
   USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
   OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
   SUCH DAMAGE.
</pre>

### unbuffer

<pre class="lic">
<a class="ext" href="http://sourceforge.net/projects/expect/packages/Expect/5.45/expect5.45.tar.gz/download">http://sourceforge.net/projects/expect/packages/Expect/5.45/expect5.45.tar.gz/download</a>

Author: Don Libes, NIST

Expect FAQ
<br /><a class="ext" href="http://expect.sourceforge.net/FAQ.html#q5">http://expect.sourceforge.net/FAQ.html#q5</a>

#5. Do we need to pay or ask for permission to distribute Expect?

It is my understanding that you are allowed to do just about anything
with Expect.  You can even sell it.  You need not ask our permission.
You need not pay for it.  (Your tax dollars, in effect, already have
paid for it.)

You should not claim that you wrote it (since this would be a lie), nor
should you attempt to copyright it (this would be fruitless as it is a
work of the US government and therefore not subject to copyright).

NIST would appreciate any credit you can give for this work.  One line
may suffice (as far as I'm concerned) although there should be
something to the effect that this software was produced for research
purposes.  No warantee, guarantee, or liability is implied.
</pre>
